#include "graph.h"
#include <iostream>
#include <fstream>
#include <regex>

using namespace std;

Graph::Graph()
{
    auto [nb_vertices, nb_edges] = ReadNbEdgesVertices("graph_repr");
    nb_edges_ = nb_edges;
    nb_vertices_ = nb_vertices;
    edges_ =
    {
        {{"one", false}},
        {{"two", false}}
    };
    AddEdge("two", "toto", false);
    PrintGraph();
}

void Graph::AddEdge(string start_vertex, string end_vertex, bool val)
{
    for (int i = 0; i < edges_.size(); i++) {
        if (get<0>(edges_[i][0]) == start_vertex) {
            edges_[i].push_back(make_tuple(end_vertex, val));
        }
    }
}

void Graph::PrintGraph()
{
    cout << "____________________" << endl;
    for (auto edge_list : edges_)
    {
        for (auto edge : edge_list)
        {
            cout << get<0>(edge) << "/" << get<1>(edge) << "-" ;
        }
        cout << endl;
    }
    cout << "____________________" << endl;
}


tuple<int, int> Graph::ReadNbEdgesVertices(string path_graph_repr)
{
    string line;
    ifstream graph_repr (path_graph_repr);
    int nb_v = -1;
    int nb_e = -1;

    if (graph_repr.is_open())
    {
        getline (graph_repr, line);
        regex number_reg("\\d+");
        sregex_iterator number = sregex_iterator(line.begin(), line.end(), number_reg);
        sregex_iterator iter_end = sregex_iterator();

        for (sregex_iterator i = number; i != iter_end; ++i)
        {
            smatch match = *i;
            string match_str = match.str();

            if (nb_v == -1)
            {
                nb_v = stoi(match_str);
            }
            else
            {
                nb_e = stoi(match_str);
            }
        }

        graph_repr.close();
    }

    else cout << "Unable to open file";
    return {nb_v, nb_e};
}

Graph::~Graph()
{
}
