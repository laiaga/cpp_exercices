#ifndef GRAPH_H
#define GRAPH_H

#include <tuple>
#include <string>
#include <vector>

class Graph
{
    public:
        virtual ~Graph();
        Graph();
        void PrintGraph();

    protected:

    private:
        int nb_edges_;
        int nb_vertices_;
        std::vector<std::vector<std::tuple<std::string, bool>>> edges_;
        std::tuple<int, int> ReadNbEdgesVertices(std::string);
        void AddEdge(std::string, std::string, bool);
};

#endif // GRAPH_H
