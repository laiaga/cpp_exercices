#include <iostream>
#include <regex>
#include <random>

using namespace std;

int main()
{
    regex die_format_reg("([1-9]\\d*)d([1-9]\\d*)");
    regex number_format_reg("[1-9]\\d*");

    string die_repr;

    while (die_repr != "stop") {

        cout << "Type a string or 'stop' to stop the program" << endl;
        cin >> die_repr;

        if (die_repr == "stop") {
            break;
        }
        if (!regex_match(die_repr, die_format_reg))
        {
            cout << "Unrecognized dice value '" << die_repr << "', please try again!" << endl;
            continue;
        }

        sregex_iterator number = sregex_iterator(die_repr.begin(), die_repr.end(), number_format_reg);
        sregex_iterator iter_end = sregex_iterator();

        int nb_die = 0;
        int dice_value = 0;

        for (sregex_iterator i = number; i != iter_end; ++i) {
            smatch match = *i;
            string match_str = match.str();

            if (nb_die == 0) {
                nb_die = stoi(match_str);
            } else {
                dice_value = stoi(match_str);
            }
        }


        random_device rd;
        default_random_engine rand_engine(rd());
        uniform_int_distribution<int> dist(1, dice_value);
        int dice_result = 0;
        for (int i=0 ; i<nb_die ; ++i) {
            dice_result += dist(rand_engine);
        }
        cout << dice_result << endl;

    }

    return 0;
}
