At the moment of writing ideas are from [Reddit's dailyprogrammer's thread](https://www.reddit.com/r/dailyprogrammer/).

Implemented problemes:
* [dice roller](https://www.reddit.com/r/dailyprogrammer/comments/8s0cy1/20180618_challenge_364_easy_create_a_dice_roller/) (easy)

Ongoing:
* [Graph of Thrones](https://www.reddit.com/r/dailyprogrammer/comments/aqwvxo/20190215_challenge_375_hard_graph_of_thrones/) (hard) graph of characters structurally balanced -> see [this page](https://www.geeksforgeeks.org/graph-and-its-representations/) for graph representation

TODO:
* [Havel-Hakimi](https://www.reddit.com/r/dailyprogrammer/comments/bqy1cf/20190520_challenge_378_easy_the_havelhakimi/) (easy) check consistency of a graph
* [Chessboard/prisonners puzzle](https://www.reddit.com/r/dailyprogrammer/comments/hrujc5/20200715_challenge_385_intermediate_the_almost/) (intermediate)
